
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ViewStudent
 */
@WebServlet("/ViewStudent")
public class ViewStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		response.setContentType("text/html");
//		HttpSession se=request.getSession();
//		if(se.getAttribute("id")==null)
//		{
//      		response.sendRedirect("index.html");
//			out.println(se.getAttribute("id"));
//		}
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/feemanagement","root","xplor");
			String qr="select * from addstudent";
			Statement st=con.createStatement();
			ResultSet rs=st.executeQuery(qr);
			if(rs.next())
			{
				out.println("<table align='center' border='1px'>");
				do
				{
					String roll=rs.getString("roll");
					String name=rs.getString("name");
					String contact=rs.getString("contact");
					String address=rs.getString("address");
					String feedue=rs.getString("feedue");
					out.println("<tr>");
					out.println("<form action=UpdateStudent>");
       			    out.println("<td>");
       			    out.println("<input type=text name=roll value="+roll+">");
       			    out.println("</td>");
      			    out.println("<td>");
       			    out.println("<input type=text name=name value="+name+">");
       			    out.println("</td>");
       			    out.println("<td>");
       			    out.println("<input type=text name=contact value="+contact+">");
       			    out.println("</td>");
      			    out.println("<td>");
      			    out.println("<input type=text name=address value="+address+">");
      			    out.println("</td>");
      			    out.println("<td>");
      			    out.println("<input type=text name=feedue value="+feedue+">");
      			    out.println("</td>");
      			    out.println("<td>");
					out.println("<button>Update</button");
					out.println("</td>");
      			    out.println("<td>");
					out.println("<a href=DeleteStudent?roll="+roll+">Delete</a>");
					out.println("</td>");
      			  out.println("</form>");
       			 out.println("</tr>");
				}while(rs.next());
				 out.println("</table>");
				 RequestDispatcher rd=request.getRequestDispatcher("accountant.html");
	       			rd.include(request, response);
			}
		} catch (Exception e) {
	         out.println(e);
		}
	}

}
